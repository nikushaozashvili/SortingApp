import java.util.Arrays;

/**
 * It is a small Java application that takes up to ten command-line arguments as integer values,
 * sorts them in the ascending order, and then prints them into standard output.
 */
public class SortingApp {
    public static final int length = 10;

    /**
     * It's main method where application starts.
     * There is written validation of command-line arguments.
     * If arguments are correct, it sorts them and prints in ascending order.
     * @param args takes up to ten command-line arguments as integer values.
     * @throws IllegalArgumentException If there are not exactly 10 arguments as integer values, it throws IllegalArgumentException.
     */
    public static void main(String[] args) throws IllegalArgumentException{
        int [] numbers = convertToNumbers(args);
        Arrays.sort(numbers);
        printNumbers(numbers);
    }

    /**
     * This method converts command-line arguments to numbers.
     * @param args It gets 10 command-line arguments.
     * @return It returns integer array containing 10 integers from command-line.
     * @throws IllegalArgumentException If there are not exactly 10 command-line arguments as integer values, it throws IllegalArgumentException.
     */
    public static int[] convertToNumbers (final String[] args) throws IllegalArgumentException{
        int [] numbers = new int[length];
        validate(args, numbers);
        return numbers;
    }

    /**
     * It checks if command-line arguments are exactly 10 as integer values.
     * @param args 10 command-line arguments.
     * @param numbers Array where this method adds numbers from command-line.
     * @throws IllegalArgumentException If there are not exactly 10 command-line arguments as integer values, it throws IllegalArgumentException.
     */
    private static void validate(final String[] args, int[] numbers) throws IllegalArgumentException {
        if(args.length != length) {
            throw new IllegalArgumentException("Quantity of command-line arguments should be 10!");
        }

        for(int i = 0; i < length; i++) {
            try{
                int number = Integer.parseInt(args[i]);
                numbers[i] = number;
            }catch(NumberFormatException e) {
                throw new IllegalArgumentException("Command-line arguments should be as integers!");
            }
        }
    }

    /**
     * It prints 10 numbers.
     * @param numbers It gets array of integers, filled with command-line arguments
     */
    public static void printNumbers(final int[] numbers) {
        for(int i = 0; i < length; i++) {
            System.out.print(numbers[i]);
            if(i != length - 1) {
                System.out.print(" ");
            }
        }
    }
}
