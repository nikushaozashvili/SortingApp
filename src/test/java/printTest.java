import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.util.Arrays;
import java.util.Collection;
import static org.junit.Assert.assertEquals;

/**
 * Parametrized tests for printNumbers method.
 */
@RunWith(Parameterized.class)
public class printTest {
    private final int[] numbers;
    private final String expected;

    public printTest(int [] numbers, String expected) {
        this.numbers = numbers;
        this.expected = expected;
    }

    /**
     * Different number cases.
     * @return returns different cases.
     */
    @Parameterized.Parameters
    public static Collection<Object[]> data () {
        return Arrays.asList(new Object[][]{
                {new int [] {999, 9, 99, 9999, 9999, 99999, 99, 9, 9, 999}, "999 9 99 9999 9999 99999 99 9 9 999"},
                {new int [] {10, 5, 3, 7, 9, 9, 4, 3, -100, 150}, "10 5 3 7 9 9 4 3 -100 150"},
                {new int [] {10000, 5000, 3000, 7000, 9000, 9000, 4000, 3000, -100000, 150000}, "10000 5000 3000 7000 9000 9000 4000 3000 -100000 150000"},
                {new int [] {0, 0, 0, 0, 0, 0, 0, 0, 0, 0}, "0 0 0 0 0 0 0 0 0 0"}

        });

    }

    @Test
    public void testPrint() {
        ByteArrayOutputStream os = new ByteArrayOutputStream();
        PrintStream ps = new PrintStream(os);
        System.setOut(ps);
        SortingApp.printNumbers(numbers);
        String result = os.toString();
        assertEquals(expected, result);
    }

}