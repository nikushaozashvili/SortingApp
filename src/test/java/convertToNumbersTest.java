import org.junit.Test;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.util.Arrays;

import static org.junit.Assert.assertArrayEquals;

/**
 * Tests for convertToNumbers method.
 */
public class convertToNumbersTest {

    /**
     * Case when there aren't any command-line arguments.
     */
    @Test(expected = IllegalArgumentException.class)
    public void lessLengthInputTest0() {
        ByteArrayOutputStream os = new ByteArrayOutputStream();
        PrintStream ps = new PrintStream(os);
        System.setOut(ps);
        String[] args = new String[0];
        SortingApp.convertToNumbers(args);
    }

    /**
     * Case when there are only 1 command-line argument.
     */
    @Test(expected = IllegalArgumentException.class)
    public void lessLengthInputTest1() {
        ByteArrayOutputStream os = new ByteArrayOutputStream();
        PrintStream ps = new PrintStream(os);
        System.setOut(ps);
        String[] args = new String[1];
        args[0] = "5";
        SortingApp.convertToNumbers(args);
    }


    /**
     * Case when there are less than 10 argument.
     */
    @Test(expected = IllegalArgumentException.class)
    public void lessLengthInputTest2() {
        ByteArrayOutputStream os = new ByteArrayOutputStream();
        PrintStream ps = new PrintStream(os);
        System.setOut(ps);
        String[] args = new String[5];
        args[0] = "5";
        args[1] = "4";
        args[2] = "150";
        args[3] = "15151";
        args[4] = "151515";
        SortingApp.convertToNumbers(args);
    }

    /**
     * Another case when there are less than 10 argument.
     */
    @Test(expected = IllegalArgumentException.class)
    public void lessLengthInputTest3() {
        ByteArrayOutputStream os = new ByteArrayOutputStream();
        PrintStream ps = new PrintStream(os);
        System.setOut(ps);
        String[] args = new String[9];
        args[0] = "5";
        args[1] = "4";
        args[2] = "150";
        args[3] = "15151";
        args[4] = "151515";
        args[5] = "5535";
        args[6] = "443243";
        args[7] = "14234";
        args[8] = "151451";
        SortingApp.convertToNumbers(args);
    }


    /**
     * Case when there are more than 10 argument.
     */
    @Test(expected = IllegalArgumentException.class)
    public void lessLengthInputTest4() {
        ByteArrayOutputStream os = new ByteArrayOutputStream();
        PrintStream ps = new PrintStream(os);
        System.setOut(ps);
        String[] args = new String[11];
        args[0] = "5";
        args[1] = "4";
        args[2] = "150";
        args[3] = "15151";
        args[4] = "151515";
        args[5] = "5535";
        args[6] = "443243";
        args[7] = "14234";
        args[8] = "151451";
        args[9] = "4145151";
        args[10] = "141414";
        SortingApp.convertToNumbers(args);
    }

    /**
     * Case when there are exactly 10 arguments, but they aren't all numbers.
     */
    @Test(expected = IllegalArgumentException.class)
    public void argumentsNotNumbersTest1() {
        ByteArrayOutputStream os = new ByteArrayOutputStream();
        PrintStream ps = new PrintStream(os);
        System.setOut(ps);
        String[] args = new String[10];
        args[0] = "5";
        args[1] = "4";
        args[2] = "150";
        args[3] = "15151";
        args[4] = "151515";
        args[5] = "5535";
        args[6] = "443243";
        args[7] = "14234";
        args[8] = "151451";
        args[9] = "Epam";
        SortingApp.convertToNumbers(args);
    }
    /**
     * Another case when there are exactly 10 arguments, but they aren't all numbers.
     */
    @Test(expected = IllegalArgumentException.class)
    public void argumentsNotNumbersTest2() {
        ByteArrayOutputStream os = new ByteArrayOutputStream();
        PrintStream ps = new PrintStream(os);
        System.setOut(ps);
        String[] args = new String[10];
        args[0] = "Epam";
        args[1] = "4";
        args[2] = "150";
        args[3] = "i";
        args[4] = "151515";
        args[5] = "k";
        args[6] = "443243";
        args[7] = "14234";
        args[8] = "151451";
        args[9] = "Epam";
        SortingApp.convertToNumbers(args);
    }

    /**
     * Case when there are exactly 10 number arguments, all equal to each other.
     */
    @Test
    public void correctInputTest1() {
        ByteArrayOutputStream os = new ByteArrayOutputStream();
        PrintStream ps = new PrintStream(os);
        System.setOut(ps);
        String[] args = new String[10];
        args[0] = "1";
        args[1] = "1";
        args[2] = "1";
        args[3] = "1";
        args[4] = "1";
        args[5] = "1";
        args[6] = "1";
        args[7] = "1";
        args[8] = "1";
        args[9] = "1";

        int [] expected = new int [SortingApp.length];
        Arrays.fill(expected,1);
        assertArrayEquals(expected, SortingApp.convertToNumbers(args));
    }

    /**
     * Case when there are exactly 10 number arguments, already sorted.
     */
    @Test
    public void correctInputTest2() {
        ByteArrayOutputStream os = new ByteArrayOutputStream();
        PrintStream ps = new PrintStream(os);
        System.setOut(ps);
        String[] args = new String[10];
        args[0] = "0";
        args[1] = "1";
        args[2] = "2";
        args[3] = "3";
        args[4] = "4";
        args[5] = "5";
        args[6] = "6";
        args[7] = "7";
        args[8] = "8";
        args[9] = "9";

        int [] expected = new int [SortingApp.length];
        for(int i = 0; i < expected.length; i++) {
            expected[i] = i;
        }
        assertArrayEquals(expected, SortingApp.convertToNumbers(args));
    }

    /**
     * Another case when there are exactly 10 number arguments.
     */
    @Test
    public void correctInputTest3() {
        ByteArrayOutputStream os = new ByteArrayOutputStream();
        PrintStream ps = new PrintStream(os);
        System.setOut(ps);
        String[] args = new String[10];
        args[0] = "9";
        args[1] = "15";
        args[2] = "-5";
        args[3] = "150";
        args[4] = "250";
        args[5] = "105";
        args[6] = "-300";
        args[7] = "-1";
        args[8] = "0";
        args[9] = "999";

        int [] expected = new int [SortingApp.length];
        expected[0] = 9;
        expected[1] = 15;
        expected[2] = -5;
        expected[3] = 150;
        expected[4] = 250;
        expected[5] = 105;
        expected[6] = -300;
        expected[7] = -1;
        expected[8] = 0;
        expected[9] = 999;
        assertArrayEquals(expected, SortingApp.convertToNumbers(args));
    }
}