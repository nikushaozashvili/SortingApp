import org.junit.runner.RunWith;
import org.junit.runners.Suite;

/**
 * Suite class for SortingApp Application.
 * There you can run tests of all methods with one click.
 */
@RunWith(Suite.class)

@Suite.SuiteClasses({
        convertToNumbersTest.class,
        printTest.class,
        uniteTest.class,
})

public class SortingAppSuite {

}
