import org.junit.Test;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

import static org.junit.Assert.assertEquals;

/**
 * Tests for whole application.
 */
public class uniteTest {
    /**
     * It tests main method with 10 arguments, one of them is not number, so main method throws IllegalArgumentException.
     */
    @Test(expected = IllegalArgumentException.class)
    public void testMain1() {
        ByteArrayOutputStream os = new ByteArrayOutputStream();
        PrintStream ps = new PrintStream(os);
        System.setOut(ps);
        String[] args = new String[10];
        args[0] = "5";
        args[1] = "4";
        args[2] = "150";
        args[3] = "15151";
        args[4] = "151515";
        args[5] = "5535";
        args[6] = "443243";
        args[7] = "14234";
        args[8] = "151451";
        args[9] = "Epam";
        SortingApp.main(args);
    }

    /**
     * It tests main method with 10 arguments, all of them numbers, so it sorts them and prints in ascending order.
     */
    @Test
    public void testMain2() {
        ByteArrayOutputStream os = new ByteArrayOutputStream();
        PrintStream ps = new PrintStream(os);
        System.setOut(ps);
        String[] args = new String[10];
        args[0] = "150";
        args[1] = "55";
        args[2] = "155";
        args[3] = "15151";
        args[4] = "-151515";
        args[5] = "-5535";
        args[6] = "443243";
        args[7] = "-14234";
        args[8] = "151451";
        args[9] = "0";
        SortingApp.main(args);
        String result = os.toString();
        assertEquals(result, "-151515 -14234 -5535 0 55 150 155 15151 151451 443243");
    }
}
